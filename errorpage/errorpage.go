package errorpage

import (
	"html/template"
	"io"
	"log"
	"net/http"
)

type errorTemplate interface {
	Execute(io.Writer, interface{}) error
}

var tmpl *template.Template

func init() {
	tmplData, err := Asset("assets/error.tmpl")
	if err != nil {
		log.Fatal(err)
	}

	tmpl, err = template.New("Error").Parse(string(tmplData))
	if err != nil {
		log.Fatal(err)
	}
}

type page struct {
	StatusCode        int
	StatusExplanation string
	Title             string
}

func (p page) writeResponse(res http.ResponseWriter, tmpl errorTemplate) {
	res.WriteHeader(p.StatusCode)
	res.Header().Set("Content-Type", "text/html; charset=UTF-8")

	err := tmpl.Execute(res, p)
	if err != nil {
		res.Header().Set("Content-Type", "text/plain")
		res.Write([]byte(p.Title))
		return
	}
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	notFound := page{StatusCode: 404, StatusExplanation: "Page not found", Title: "Page not found"}
	notFound.writeResponse(w, tmpl)
}
