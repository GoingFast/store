package response

import (
	"encoding/json"
	"net/http"
)

var (
	ErrInternal         = "an internal server error has occured. Please contact technical support"
	ErrMalformedPayload = "JSON payload is malformed"
)

type SuccessError struct {
	Err string `json:"err,omitempty"`
	Msg string `json:"msg,omitempty"`
}

func JSON(w http.ResponseWriter, code int, v interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf8")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(&v)
}

func Decode(r *http.Request, v interface{}) error {
	return json.NewDecoder(r.Body).Decode(&v)
}
