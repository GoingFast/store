package logger

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/GoingFast/stream/utils/response"
	sentry "github.com/getsentry/raven-go"
	"github.com/olivere/elastic"
)

type (
	logData struct {
		Username  string    `json:"username,omitempty"`
		IP        string    `json:"ip,omitempty"`
		Timestamp time.Time `json:"timestamp"`
		Action    string    `json:"action"`
		Proto     string    `json:"proto"`
		Method    string    `json:"method"`
		UserAgent string    `json:"useragent"`
		Host      string    `json:"host"`
		URL       string    `json:"url"`
	}

	logger struct {
		elastic *elastic.Client
		sentry  *sentry.Client
	}

	// Log is an interface that wraps the logging methods
	Log interface {
		SetupElastic(string) error
		GeneralLog(*http.Request, string)
		ErrorLog(error, string, http.ResponseWriter)
	}
)

// NewLogger returns a new instance of logger
func NewLogger(e *elastic.Client, s *sentry.Client) *logger {
	return &logger{
		elastic: e,
		sentry:  s,
	}
}

// SetupElastic setups up Elasticsearch by creating index
// DRY
func (l logger) SetupElastic(indexName string) error {
	ctx := context.Background()

	exists, err := l.elastic.IndexExists(indexName).Do(ctx)
	if err != nil {
		return err
	}

	if exists {
		return nil
	}

	_, err = l.elastic.CreateIndex(indexName).Do(ctx)
	if err != nil {
		return err
	}
	return nil
}

// GeneralLog is a method that logs to Elasticsearch
// Used in cases: "user1/IP1 registered", "user2/IP2 sent a new request" etc
func (l logger) GeneralLog(r *http.Request, action string) {
	ctx := context.Background()

	host := r.Host
	proto := r.Proto
	method := r.Method
	userAgent := r.UserAgent()
	ipPort := r.RemoteAddr
	timestamp := time.Now().UTC()
	url := r.URL.String()

	ip, _, _ := net.SplitHostPort(ipPort)

	logFields := logData{
		IP:        ip,
		Action:    action,
		Timestamp: timestamp,
		Proto:     proto,
		Method:    method,
		UserAgent: userAgent,
		Host:      host,
		URL:       url,
	}

	_, err := l.elastic.Index().
		Index("logs").
		Type("_doc").
		BodyJson(logFields).
		Do(ctx)

	// shouldnt happen but just in case
	if err != nil {
		fmt.Println(err)
		l.sentry.CaptureError(err, map[string]string{"service": "logger"})
	}
}

// ErrorLog is a method that logs to Sentry
// Used in cases: unexpected errors
func (l logger) ErrorLog(err error, service string, w http.ResponseWriter) {
	trace := l.sentry.CaptureError(err, map[string]string{"service": service})
	response.JSON(w, http.StatusInternalServerError, map[string]string{
		"err":   response.ErrInternal,
		"trace": trace,
	})
}
