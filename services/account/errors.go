// Package account package holds service based errors
package account

var (
	ErrAccountExists = "account with the requested email already exists"
)
