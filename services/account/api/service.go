package account

import (
	"database/sql"
	"net/http"

	"github.com/GoingFast/store2/errorpage"
	"github.com/GoingFast/store2/helper/json"
	"github.com/GoingFast/store2/helper/logger"
	"github.com/GoingFast/store2/middleware"
	"github.com/GoingFast/store2/services"
	errors "github.com/GoingFast/store2/services/account"
	sentry "github.com/getsentry/raven-go"
	"github.com/go-chi/chi"
	"github.com/olivere/elastic"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type accountService struct {
	store   accountStore
	logrus  *logrus.Logger
	sentry  *sentry.Client
	config  model.Configuration
	elastic *elastic.Client
	logger  logger.Log
}

// NewAccountService returns a new instance of accountService
func NewAccountService(db *sql.DB, logrus *logrus.Logger, sentry *sentry.Client, config model.Configuration, elastic *elastic.Client, logger logger.Log) *accountService {
	return &accountService{
		store:   newAccountStore(db),
		logrus:  logrus,
		sentry:  sentry,
		config:  config,
		elastic: elastic,
		logger:  logger,
	}
}

// MakeHTTPHandler is responsible for the routing
func (a accountService) MakeHTTPHandler() *chi.Mux {
	r := chi.NewRouter()
	recoverer := middleware.NewRecoverer(a.logrus, a.sentry, a.config)

	r.Use(recoverer.Recoverer)
	r.NotFound(errorpage.NotFound)

	r.Route("/v1", func(r chi.Router) {
		r.Post("/account", a.CreateAccount)
	})

	return r
}

// CreateAccount is the business logic for creating a new account
func (a *accountService) CreateAccount(w http.ResponseWriter, r *http.Request) {
	var account model.Account
	err := response.Decode(r, account)
	if err != nil {
		response.JSON(w, http.StatusBadRequest, response.SuccessError{Err: response.ErrMalformedPayload})
		return
	}

	exists, err := a.store.AccountExists(account.Email)
	if err != nil {
		a.logger.ErrorLog(err, "account", w)
		return
	}

	if exists {
		response.JSON(w, http.StatusFound, response.SuccessError{Err: errors.ErrAccountExists})
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(account.Password), 3)
	if err != nil {
		response.JSON(w, http.StatusInternalServerError, response.SuccessError{Err: response.ErrInternal})
		return
	}

	account.Password = string(hashedPassword)

	err = a.store.Create(&account)
	if err != nil {
		a.logger.ErrorLog(err, "account", w)
		return
	}

	go a.logger.GeneralLog(r, "registered")

	response.JSON(w, http.StatusOK, response.SuccessError{Msg: "success"})
}
