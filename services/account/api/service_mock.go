package account

import (
	"net/http"

	"github.com/GoingFast/store2/services"
)

type mockAccountStore struct {
	OnAccountExists func(string) (bool, error)
	OnCreate        func(*model.Account) error
}

type mockLogger struct {
	OnGeneralLog   func(*http.Request, string)
	OnErrorLog     func(error, string, http.ResponseWriter)
	OnSetupElastic func(string) error
}

func (m mockAccountStore) AccountExists(email string) (bool, error) {
	return m.OnAccountExists(email)
}

func (m mockAccountStore) Create(u *model.Account) error {
	return m.OnCreate(u)
}

func (m mockLogger) GeneralLog(r *http.Request, action string) {
	m.OnGeneralLog(r, action)
}

func (m mockLogger) ErrorLog(err error, service string, w http.ResponseWriter) {
	m.OnErrorLog(err, service, w)
}

func (m mockLogger) SetupElastic(i string) error {
	return m.OnSetupElastic(i)
}
