package account

import (
	"database/sql"

	"github.com/GoingFast/store2/services"
)

type (
	accountStore interface {
		AccountExists(string) (bool, error)
		Create(*model.Account) error
	}

	accStore struct {
		*sql.DB
	}
)

func newAccountStore(db *sql.DB) *accStore {
	return &accStore{db}
}

func (ac accStore) AccountExists(email string) (ok bool, err error) {
	err = ac.QueryRow("SELECT EXISTS(SELECT 1 FROM accounts WHERE email = $1)").Scan(&ok)
	return
}

func (ac accStore) Create(account *model.Account) error {
	tx, err := ac.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO accounts (name, email, password)")
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = stmt.Exec(account.Name, account.Email, account.Password)
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}
