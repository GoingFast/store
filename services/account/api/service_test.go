package account

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/GoingFast/store2/services"
	"github.com/stretchr/testify/assert"
)

func toJSON(v interface{}) (io.Reader, error) {
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(&v)
	if err != nil {
		return nil, err
	}
	return &buf, nil
}

func Test_accountService_CreateAccount(t *testing.T) {
	type fields struct {
		store  mockAccountStore
		logger mockLogger
	}
	tests := []struct {
		name       string
		fields     fields
		wantStatus int
		wantBody   interface{}
	}{
		{
			name: "creates a new account",
			fields: fields{
				store: mockAccountStore{
					OnAccountExists: func(string) (bool, error) {
						return false, nil
					},
					OnCreate: func(*model.Account) error {
						return nil
					},
				},
				logger: mockLogger{
					OnGeneralLog: func(*http.Request, string) {
					},
					OnErrorLog: func(error, string, http.ResponseWriter) {
					},
				},
			},
			wantStatus: 200,
			wantBody:   "success",
		},
		{
			name: "should return 302 if account already exists",
			fields: fields{
				store: mockAccountStore{
					OnAccountExists: func(string) (bool, error) {
						return true, nil
					},
					OnCreate: func(*model.Account) error {
						return nil
					},
				},
				logger: mockLogger{
					OnGeneralLog: func(*http.Request, string) {
					},
					OnErrorLog: func(error, string, http.ResponseWriter) {
					},
				},
			},
			wantStatus: 302,
			wantBody:   "exists",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := accountService{
				store:  tt.fields.store,
				logger: tt.fields.logger,
			}

			j, err := toJSON(model.Account{
				Name:     "testname",
				Email:    "testmail@gmail.com",
				Password: "testpw",
			})
			assert.Nil(t, err)

			r, err := http.NewRequest("POST", "/", j)
			assert.Nil(t, err)

			w := httptest.NewRecorder()
			a.CreateAccount(w, r)

			assert.Equal(t, tt.wantStatus, w.Code)
			assert.Contains(t, w.Body.String(), tt.wantBody)
		})
	}
}
