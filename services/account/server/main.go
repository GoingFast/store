package main

import (
	"fmt"
	stdlog "log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/GoingFast/huh/helper"
	"github.com/GoingFast/store2/helper/logger"
	"github.com/GoingFast/store2/services"
	"github.com/GoingFast/store2/services/account/api"
	sentry "github.com/getsentry/raven-go"
	"github.com/oklog/oklog/pkg/group"
	"github.com/olivere/elastic"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	indexName = "logs"
)

func main() {
	logruslog := logrus.New()

	logruslog.Formatter = new(logrus.JSONFormatter)
	logruslog.Level = logrus.DebugLevel
	logruslog.Out = os.Stdout

	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		stdlog.Fatalf("viper: %v", err)
	}

	var config model.Configuration
	err = viper.Unmarshal(&config)
	if err != nil {
		stdlog.Fatalf("viper: %v", err)
	}

	db, err := helper.OpenDB("localhost")
	if err != nil {
		stdlog.Fatalf("postgres: %v", err)
	}

	_, err = db.Exec("SELECT 1")
	if err != nil {
		stdlog.Fatalf("postgres: %v", err)
	}

	elastic, err := elastic.NewClient()
	if err != nil {
		stdlog.Fatalf("elastic: %v", err)
	}

	// sclient, err := sentry.NewWithTags("https://a54a683ea0154d199816a4cab1412477:2a5790c7e894493fa0cf8e627f206768@sentry.io/1272216", map[string]string{"env": "account"})
	// if err != nil {
	// 	stdlog.Fatalf("sentry: %v", err)
	// }

	sclient, err := sentry.NewWithTags("http://6c50d2dfafd648a0ab0eb8857db9dff9:05e88560523c4bd8ad2bc821e8433f91@localhost:9000/1", map[string]string{"env": "account"})
	if err != nil {
		stdlog.Fatalf("sentry: %v", err)
	}

	defer sclient.Close()

	var log logger.Log
	{
		log = logger.NewLogger(elastic, sclient)
	}

	err = log.SetupElastic("logs")
	if err != nil {
		stdlog.Fatalf("elastic: %v", err)
	}

	accountService := account.NewAccountService(db, logruslog, sclient, config, elastic, log)
	h := accountService.MakeHTTPHandler()

	httpsrv := &http.Server{
		Addr:         config.HTTPPort,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      h,
	}

	var g group.Group
	{
		cancelInterrupt := make(chan struct{})
		g.Add(func() error {
			c := make(chan os.Signal)
			signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
			select {
			case sig := <-c:
				return fmt.Errorf("received signal: %s", sig)
			case <-cancelInterrupt:
				return nil
			}
		}, func(error) {
			close(cancelInterrupt)
		})
	}
	{
		ln, _ := net.Listen("tcp", config.HTTPPort)
		g.Add(func() error {
			fmt.Println("Server is listening")
			return httpsrv.Serve(ln)
		}, func(err error) {
			ln.Close()
			stdlog.Fatalf("Server encountered an error: %v", err)
		})
	}

	stdlog.Fatal("exit", g.Run())
}
