package model

type Server struct {
	HTTPPort    string
	Development string
}

type Configuration struct {
	Server
}

type Account struct {
	Name     string `json:"name,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
	Active   string `json:"active,omitempty"`
}
