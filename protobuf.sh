#!/usr/bin/env bash

set -e

# account
# gateway gogo swagger
protoc -I. \
    -I $GOPATH/src \
    -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    --go_out=plugins=grpc:../services/account/protobuf --grpc-gateway_out=logtostderr=true:../services/account/protobuf --swagger_out=logtostderr=true:../services/account/protobuf \
    account.proto
