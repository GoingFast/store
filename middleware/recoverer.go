package middleware

import (
	"net/http"
	"runtime"

	"github.com/GoingFast/store2/services"
	"github.com/GoingFast/stream/utils/response"
	sentry "github.com/getsentry/raven-go"
	"github.com/sirupsen/logrus"
)

type Recoverer struct {
	Logger *logrus.Logger
	Sentry *sentry.Client
	Config model.Configuration
}

func NewRecoverer(logger *logrus.Logger, sentry *sentry.Client, config model.Configuration) *Recoverer {
	return &Recoverer{logger, sentry, config}
}

func (rec *Recoverer) Recoverer(n http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if rec.Config.Development == "1" {
			defer func() {
				err := recover()
				if err != nil {

					stack := make([]byte, 1024*8)
					stack = stack[:runtime.Stack(stack, false)]

					rec.Logger.WithFields(logrus.Fields{
						"stack": string(stack),
						"err":   err,
					}).Error("Recovered from panic")

					response.JSON(w, http.StatusInternalServerError, map[string]string{
						"err": response.ErrInternal,
					})

				}
			}()

			n.ServeHTTP(w, r)

		} else {

			err, traceid := rec.Sentry.CapturePanicAndWait(func() {
				n.ServeHTTP(w, r)
			}, map[string]string{"service": "account"})

			if err != nil {

				response.JSON(w, http.StatusInternalServerError, map[string]string{
					"err":     response.ErrInternal,
					"traceid": traceid,
				})

			}
		}
	}

	return http.HandlerFunc(fn)
}
